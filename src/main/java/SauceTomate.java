public class SauceTomate extends SauceDecorator{

    public SauceTomate(Pizza pizza){
        super(pizza);
    }

    public double getPrix() {
        return super.getPrix() +500;
    }


    public String getNom() {
        return super.getNom() +"avec sauce tomate";
    }
    public String getVariante() {
        return super.getVariante() +"avec sauce tomate";
    }

}
