public abstract class SauceDecorator implements Pizza {
    protected Pizza pizza;

    public SauceDecorator(Pizza pizza){
        this.pizza= pizza;
    }

    public double getPrix() {
        return pizza.getPrix();
    }


    public String getNom() {
        return pizza.getNom();
    }

    public String getTaille(String taille){
        return  pizza.getTaille(taille);
    }

    public String getVariante() {
        return pizza.getVariante();
    }
}
