public interface Pizza {

    double getPrix();
    String getNom();
    String getVariante();
    String getTaille(String taille);

}
