public class app {
    public static void main(String[] args){
        Pizza pizza = new ConcretePizza();
        System.out.println("Je viens d'acheter une "+pizza.getNom()+", "+pizza.getVariante()+", a "+pizza.getPrix());
        pizza = new SauceBolognaise(pizza);
        //pizza = new SauceTomate(pizza);
        //System.out.println("Apres appel decorator sauce tomate sur la pizza concrete, on a : "+pizza.getNom()+", "+pizza.getVariante()+", a "+pizza.getPrix());
        System.out.println("Apres appel decorator sauce bolognaise sur la pizza concrete, on a : "+pizza.getNom()+", "+pizza.getVariante()+", a "+pizza.getPrix());

    }
}
