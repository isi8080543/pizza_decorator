public class SauceBolognaise extends SauceDecorator{

    public SauceBolognaise(Pizza pizza){
        super(pizza);
    }

    public double getPrix() {
        return super.getPrix() +1000;
    }
    public String getNom() {
        return super.getNom() +"avec Sauce Bolognaise";
    }
    public String getVariante() {
        return super.getVariante() +"avec Sauce Bolognaise";
    }
}
